import { Component } from '@angular/core';
import { Note } from './note.model';
import { NotesService } from './notes.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-keep-level-1-assignment';
  newNote: Note = new Note();
  notes: Note[] = [];
  errMessage: string = '';

  constructor(private notesService: NotesService) { }

  ngOnInit() {
    this.loadNotes();
  }

  loadNotes() {
    this.notesService.getNotes()
      .subscribe(
        notes => this.notes = notes,
        error => this.errMessage = 'Error loading notes.'
      );
  }

  addNote() {
    this.notesService.addNote(this.newNote)
      .subscribe(
        newNote => {
          this.notes.push(newNote);
          this.newNote = new Note();
        },
        error => this.errMessage = 'Error adding note.'
      );
  }

}
